
const mdWebSocketClient = function (hostFullURL, wsHostFullURL) {
	var thisMDWSClass = this;
	thisMDWSClass.host = hostFullURL;
	thisMDWSClass.sessionId = '';
	thisMDWSClass.sessionIdExpires = new Date();
	thisMDWSClass.wsHost = wsHostFullURL;
	thisMDWSClass.ws = null;
	thisMDWSClass.wsIsOnline = false;
	thisMDWSClass.timeoutVariable = null;

	thisMDWSClass.responseActions = {};

	thisMDWSClass.login = function (requestObject) {

		var dataObject = {};
		var onSuccess = function () { };
		var onError = function () { };
		var onFinished = function () { };
		if (requestObject.hasOwnProperty('data')) {
			dataObject = requestObject.data;
		}

		if (requestObject.hasOwnProperty('username')) {
			dataObject.username = requestObject.username;
		}

		if (requestObject.hasOwnProperty('password')) {
			dataObject.password = requestObject.password;
		}

		if (requestObject.hasOwnProperty('device')) {
			dataObject.device = requestObject.device;
		}

		if (requestObject.hasOwnProperty('deviceType')) {
			dataObject.deviceType = requestObject.deviceType;
		}

		if (requestObject.hasOwnProperty('deviceF7')) {
			dataObject.deviceF7 = requestObject.deviceF7;
		}

		if (requestObject.hasOwnProperty('buildInfo')) {
			dataObject.buildInfo = requestObject.buildInfo;
		}

		if (requestObject.hasOwnProperty('deviceNavigator')) {
			dataObject.deviceNavigator = requestObject.deviceNavigator;
		}

		if (requestObject.hasOwnProperty('saveLogin')) {
			dataObject.saveLogin = requestObject.saveLogin;
		}

		if (requestObject.hasOwnProperty('onSuccess')) {
			onSuccess = requestObject.onSuccess;
		}

		if (requestObject.hasOwnProperty('onError')) {
			onError = requestObject.onError;
		}

		if (requestObject.hasOwnProperty('onFinished')) {
			onFinished = requestObject.onFinished;
		}

		thisMDWSClass.ajaxToServer({
			method: 'POST',
			showSpinner: true,
			url: '/4DACTION/web_auth',
			data: dataObject,
			timeout: 8000,
			success: function (data) {
				if ((data.ok) || (data.verifyDevice)) {
					thisMDWSClass.sessionId = data.sessionId;
					thisMDWSClass.sessionIdExpires = new Date(data.sessionEndDateTime);
					onSuccess(data);
					onFinished();
				} else {
					if ((!data.error) || (data.error == '')) {
						data.error = 'Login failed.'
					}
					onError(data);
					onFinished();
				}
			},
			error: function (data) {
				if ((!data.error) || (data.error == '')) {
					data.error = 'Login failed.'
				}
				onError(data);
				onFinished();
			}
		});
	};

	thisMDWSClass.logout = function () {
		thisMDWSClass.sessionId = '';
		thisMDWSClass.closeWebSocket();
		for (let prop in thisMDWSClass.responseActions) {
			delete thisMDWSClass.responseActions[prop];
		}
		// TODO: thisMDWSClass.ajaxToServer logout...
	};

	thisMDWSClass.ajaxToServer = function (requestObject) {
		requestObject = ((typeof (requestObject) == 'object') ? requestObject : {});

		var requestURL = '';
		var timeout = 10000;
		var dataObject = {};
		var showAjaxIndicator = true; // Default
		var cache = false;
		var beforeSendFunction = function () { };
		var completeFunction = function () { };
		var successFunction = function () { };
		// TODO: timeout function?
		var errorFunction = function (data) { console.log('Error', data); };
		var requestMethod = 'GET';

		// TODO: how to use... thisMDWSClass.sessionIdExpires

		if (requestObject.hasOwnProperty('url')) { // REQUIRED
			requestURL = requestObject.url;

			if (requestObject.hasOwnProperty('data')) {
				dataObject = requestObject.data;
			}

			if (requestObject.hasOwnProperty('showSpinner')) {
				showAjaxIndicator = requestObject.showSpinner;
			}

			if (requestObject.hasOwnProperty('timeout')) {
				timeout = requestObject.timeout;
			}

			if (requestObject.hasOwnProperty('cache')) {
				cache = requestObject.cache;
			}

			if (!cache) { // Force it...
				dataObject.noCache = Math.floor(Math.random() * 999999999);

			}

			// TODO: clean up showAjaxIndicator...
			if (requestObject.hasOwnProperty('beforeSend')) {
				if (showAjaxIndicator) {
					beforeSendFunction = function (xhr) {
						window.app.preloader.show();
						return (requestObject.beforeSend(xhr));
					}
				} else {
					beforeSendFunction = requestObject.beforeSend;
				}
			} else {
				if (showAjaxIndicator) {
					beforeSendFunction = function (xhr) {
						window.app.preloader.show();
						return true;
					}
				}
			}

			// TODO: clean up showAjaxIndicator...
			if (requestObject.hasOwnProperty('complete')) {
				if (showAjaxIndicator) {
					completeFunction = function (xhr, status) {
						requestObject.complete(xhr, status);
						window.app.preloader.hide();
					}
				} else {
					completeFunction = requestObject.complete;
				}
			} else {
				if (showAjaxIndicator) {
					completeFunction = function (xhr, status) {
						window.app.preloader.hide();
					}
				}
			}

			if (requestObject.hasOwnProperty('success')) {
				successFunction = requestObject.success;
			}

			if (requestObject.hasOwnProperty('error')) {
				errorFunction = requestObject.error;
			}

			if (requestObject.hasOwnProperty('method')) {
				requestMethod = requestObject.method;
			}

			var serverURL = this.host;
			if (requestURL.indexOf("http") > -1) {
				serverURL = requestURL;
			} else {
				serverURL += requestURL;
			}

			dataObject.sessionId = thisMDWSClass.sessionId; // necessary? Yes!

			var tempCallBack = function (data) {
				successFunction(data);
			};

			var requestOptions = {
				url: serverURL,
				method: requestMethod,
				data: dataObject,//requestMethod == 'GET' ? dataObject : JSON.stringify(dataObject),
				dataType: 'json',
				headers: {
					'X-Requested-With': 'XMLHttpRequest'
				},
				beforeSend: beforeSendFunction,
				complete: completeFunction,
				success: tempCallBack,
				error: errorFunction,
				timeout: timeout,
				xhrFields: {
					withCredentials: false
				}
			};

			if (requestMethod == 'POST') { // TODO: don't hardcode this
				requestOptions.contentType = 'application/json';
			}

			window.app.request(requestOptions);

		} else {
			errorFunction({
				ok: false,
				error: 'URL required for ajaxToServer Calls'
			});

		}
	}

	thisMDWSClass.sendOverWebSocket = function (dataObject, eventsHookObject) {
		// TODO: this is a JSON OBJECT specific send...

		eventsHookObject = ((typeof (eventsHookObject) == 'undefined') ? {} : eventsHookObject); // thisMDWSClass.responseActions

		var messageSentSuccessfully = false;
		var errorMessage = '';

		if (thisMDWSClass.ws) {
			if (thisMDWSClass.ws.readyState == thisMDWSClass.ws.OPEN) {

				// console.log('eventsHookObject',eventsHookObject);
				if (!(Object.keys(eventsHookObject).length === 0)) {
					// console.log('GOT HEREeventsHookObject',eventsHookObject);
					var thisUniqueSendId = '';
					var attempts = 0;
					do {
						//console.log('StartedLoop',thisUniqueSendId);
						thisUniqueSendId = 'mdWS_' + (new Date().getTime()) + '_' + Math.random();  // TODO based off date and random number...
						//console.log('EndingLoop',thisUniqueSendId);
						attempts++;
					} while ((thisMDWSClass.responseActions.hasOwnProperty(thisUniqueSendId)) && (attempts < 5));

					dataObject.mdWebSocketUniqueIdForRequest = thisUniqueSendId;

					thisMDWSClass.responseActions[thisUniqueSendId] = '';

					if (eventsHookObject.hasOwnProperty('onReceive')) {
						thisMDWSClass.responseActions[thisUniqueSendId + '_onReceive'] = eventsHookObject.onReceive;
						if (thisMDWSClass.responseActions[thisUniqueSendId] !== '') {
							thisMDWSClass.responseActions[thisUniqueSendId] += ',';
						}
						thisMDWSClass.responseActions[thisUniqueSendId] += 'onReceive';
					}
					if (eventsHookObject.hasOwnProperty('onSuccess')) {
						thisMDWSClass.responseActions[thisUniqueSendId + '_onSuccess'] = eventsHookObject.onSuccess;
						if (thisMDWSClass.responseActions[thisUniqueSendId] !== '') {
							thisMDWSClass.responseActions[thisUniqueSendId] += ',';
						}
						thisMDWSClass.responseActions[thisUniqueSendId] += 'onSuccess';
					}
					if (eventsHookObject.hasOwnProperty('onError')) {
						thisMDWSClass.responseActions[thisUniqueSendId + '_onError'] = eventsHookObject.onError;
						if (thisMDWSClass.responseActions[thisUniqueSendId] !== '') {
							thisMDWSClass.responseActions[thisUniqueSendId] += ',';
						}
						thisMDWSClass.responseActions[thisUniqueSendId] += 'onError';
					}
					if (eventsHookObject.hasOwnProperty('onFinished')) {
						thisMDWSClass.responseActions[thisUniqueSendId + '_onFinished'] = eventsHookObject.onFinished;
						if (thisMDWSClass.responseActions[thisUniqueSendId] !== '') {
							thisMDWSClass.responseActions[thisUniqueSendId] += ',';
						}
						thisMDWSClass.responseActions[thisUniqueSendId] += 'onFinished';
					}

					dataObject.mdWebSocketUniqueIdForRequestHooks = thisMDWSClass.responseActions[thisUniqueSendId];
					// console.log('thisMDWSClass.responseActions',thisMDWSClass.responseActions);
				}

				thisMDWSClass.ws.send(JSON.stringify(dataObject));

				messageSentSuccessfully = true;
			} else {
				errorMessage = 'WebSocket is not open/ready.';
				// TODO: fallback...? Send a POST...?

			}
		} else {
			errorMessage = 'WebSocket variable not initialized.';
		}

		if ((!Object.keys(eventsHookObject).length === 0) && eventsHookObject.constructor === Object) {
			if (!messageSentSuccessfully) {
				if (eventsHookObject.hasOwnProperty('onError')) { // And is a function...
					eventsHookObject.onError(errorMessage);
				}
			}
		}

		return messageSentSuccessfully;
	};

	thisMDWSClass.connectWebSocket = function (paramsObject) {
		paramsObject = ((typeof (paramsObject) == 'undefined') ? {} : paramsObject);

		if (!paramsObject.hasOwnProperty('autoReconnectOnDisconnect')) {
			paramsObject.autoReconnectOnDisconnect = true; // default...
		}
		if (!paramsObject.hasOwnProperty('forceReconnect')) { // If already connected...
			paramsObject.forceReconnect = false; // default...
		}
		if (!paramsObject.hasOwnProperty('onMessage')) {
			paramsObject.onMessage = function () { };
		}
		if (!paramsObject.hasOwnProperty('onOpen')) {
			paramsObject.onOpen = function () { };
		}
		if (!paramsObject.hasOwnProperty('onClose')) {
			paramsObject.onClose = function () { };
		}
		if (!paramsObject.hasOwnProperty('onTimeout')) {
			paramsObject.onTimeout = function () { };
		}
		if (!paramsObject.hasOwnProperty('onAlreadyConnected')) {
			paramsObject.onAlreadyConnected = function () { };
		}
		if (!paramsObject.hasOwnProperty('timeout')) {
			paramsObject.timeout = 2000;
		}

		if ((paramsObject.forceReconnect)
			|| (!thisMDWSClass.ws)
			|| (!thisMDWSClass.ws.readyState)
			|| (thisMDWSClass.ws.readyState !== WebSocket.OPEN)) {

			if (thisMDWSClass.ws !== null) {
				thisMDWSClass.ws.close();
			}

			if (thisMDWSClass.sessionId !== '') {
				if (typeof (ReconnectingWebSocket) !== 'undefined') {
					thisMDWSClass.ws = new ReconnectingWebSocket(thisMDWSClass.wsHost, ['json', thisMDWSClass.sessionId]);
				} else {
					thisMDWSClass.ws = new WebSocket(thisMDWSClass.wsHost, ['json', thisMDWSClass.sessionId]);
				}

				thisMDWSClass.ws.onopen = function (e) {
					thisMDWSClass.wsIsOnline = true;
					if (thisMDWSClass.timeoutVariable !== null) {
						clearTimeout(thisMDWSClass.timeoutVariable);
						thisMDWSClass.timeoutVariable = null;
					}
					paramsObject.onOpen(e);
				};
				thisMDWSClass.ws.onmessage = function (e) {
					// requestIdHookResponse

					var tempData = JSON.parse(e.data);
					//window.app.rootComponent.onPushReceived();

					// TODO: need to look at a reasonable timeout/garbage cleanup for responses that don't ever come!
					// As in the server aborted and didn't send the "onFinished" hook, etc...
					if ((tempData.type) && (tempData.type == 'requestIdHookResponse')) {
						var methodString = tempData.requestId + '_' + tempData.event;
						if (thisMDWSClass.responseActions.hasOwnProperty(methodString)) {
							thisMDWSClass.responseActions[methodString](tempData);
							// After running, delete it!  TODO: keep track of hooks left...
							delete thisMDWSClass.responseActions[methodString]; // TODO: is this okay?

							if (thisMDWSClass.responseActions[tempData.requestId] = tempData.event) {
								// remove the hooks left variable...
								delete thisMDWSClass.responseActions[tempData.requestId];
							} else {
								// Then just remove this specific event...
								thisMDWSClass.responseActions[tempData.requestId] = str.replace(',' + tempData.event, '');
								thisMDWSClass.responseActions[tempData.requestId] = str.replace(tempData.event + ',', '');
							}
							// console.log('thisMDWSClass.responseActions',thisMDWSClass.responseActions);
						}
					}

					paramsObject.onMessage(e);
				},
					thisMDWSClass.ws.onclose = function (e) {

						if (thisMDWSClass.wsIsOnline) {
							thisMDWSClass.wsIsOnline = false;
							paramsObject.onClose(e);

						} else {
							// Never connected, run the timeout function...
							if (thisMDWSClass.timeoutVariable !== null) {
								clearTimeout(thisMDWSClass.timeoutVariable);
								thisMDWSClass.timeoutVariable = null;
							}

							paramsObject.onTimeout(e);

						}
					};

				if (thisMDWSClass.timeoutVariable !== null) {
					clearTimeout(thisMDWSClass.timeoutVariable);
					thisMDWSClass.timeoutVariable = null;
				}

				thisMDWSClass.timeoutVariable = setTimeout(function () {
					paramsObject.onTimeout();
				}, paramsObject.timeout);

				return true;

			} else {
				// ERROR!  Do Nothing!
				console.log('ERROR: no sessionId for WebSocket', thisMDWSClass);

			}

			return false;
		} else {
			paramsObject.onAlreadyConnected();
			//console.log('already connected...');
			return true;
		}
	};

	thisMDWSClass.closeWebSocket = function (onCloseFunction) {
		onCloseFunction = ((typeof (onCloseFunction) == 'function') ? onCloseFunction : null);

		if (thisMDWSClass.ws !== null) {
			if (onCloseFunction !== null) { // override close function...
				thisMDWSClass.ws.onclose = onCloseFunction;
			}
			thisMDWSClass.ws.close();
		}
	};

};

export default mdWebSocketClient;
