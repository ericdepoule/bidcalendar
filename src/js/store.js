import { createStore } from 'framework7';

import mdWebSocketClient from "mdWebSocketClient/index.js";
const mainWebHost = 'https://4ddevelopment.hdfowler.com';
const mainWebSocketURL = 'wss://4ddevelopment.hdfowler.com/websocket/calendarapp';

const store = createStore({
	state: {
		mdWebSocket: new mdWebSocketClient(mainWebHost, mainWebSocketURL),
		serverData: {},
		calendar: null,
		products: [
			{
				id: '1',
				title: 'Apple iPhone 8',
				description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nisi tempora similique reiciendis, error nesciunt vero, blanditiis pariatur dolor, minima sed sapiente rerum, dolorem corrupti hic modi praesentium unde saepe perspiciatis.'
			},
			{
				id: '2',
				title: 'Apple iPhone 8 Plus',
				description: 'Velit odit autem modi saepe ratione totam minus, aperiam, labore quia provident temporibus quasi est ut aliquid blanditiis beatae suscipit odio vel! Nostrum porro sunt sint eveniet maiores, dolorem itaque!'
			},
			{
				id: '3',
				title: 'Apple iPhone X',
				description: 'Expedita sequi perferendis quod illum pariatur aliquam, alias laboriosam! Vero blanditiis placeat, mollitia necessitatibus reprehenderit. Labore dolores amet quos, accusamus earum asperiores officiis assumenda optio architecto quia neque, quae eum.'
			},
		]
	},
	getters: {
		products({ state }) {
			return state.products;
		},
	},
	actions: {
		addProduct({ state }, product) {
			state.products = [...state.products, product];
		},
		sessionUpdate({ state, dispatch }, params) {
			state.session = Object.assign({}, state.session, {
				isLoggedIn: params.isLoggedIn
			});

			if (typeof (params.sessionId) !== 'undefined') {
				state.session = Object.assign({}, state.session, {
					id: params.sessionId
				});
			}

			if (typeof (params.user) !== 'undefined') {
				state.session = Object.assign({}, state.session, {
					user: params.user
				});
			}

			if (state.session.isLoggedIn) {
				// Open websocket!
				state.mdWebSocket.sessionId = state.session.id;
				dispatch('connectWebSocket');
			} else {
				// Close websocket!

			}

		},
		connectWebSocket: ({ state, dispatch }, params) => {
			params = (typeof (params) !== 'undefined') ? params : {};
			params.onOpen = (typeof (params.onOpen) !== 'undefined') ? params.onOpen : () => { };
			params.onClose = (typeof (params.onClose) !== 'undefined') ? params.onClose : () => { };
			params.onTimeout = (typeof (params.onTimeout) !== 'undefined') ? params.onTimeout : () => { };

			// window.app.preloader.show(); // If open...?

			state.mdWebSocket.connectWebSocket({
				onAlreadyConnected: () => {
					window.app.preloader.hide(); // If open...?
				},
				onOpen: () => {
					window.app.preloader.hide(); // If open...?
					window.app.loginScreen.close('#my-login-screen', false);
					document.activeElement.blur();
					let today = new Date();
					// if (!detailsLoad)
					dispatch('sendPageStatus', { first: true, selectedMonth: today.getMonth() + 1 + 2, selectedYear: today.getFullYear() });
					params.onOpen();
				},
				onMessage: (e) => {
					dispatch('onWebSocketData', JSON.parse(e.data));
				},
				onClose: () => {
					params.onClose();
				},
				onTimeout: () => {
					// error connecting...
					console.log('onTimeout called!');
					params.onTimeout();
				}
			});
		},
		onWebSocketData: ({ state, dispatch }, data) => {
			// Global data here!
			if ((data.type == 'action') && (data.action == 'logout')) {
				dispatch('logout');
			} else if ((data.type) && (data.type == 'data') && (data.hasMultipleData)) { // TODO...

			} else if ((data.type) && (data.type == 'data')) {
				state.serverData = Object.assign({}, state.serverData, {
					[data.data]: data[data.data]
				});
			}
			dispatch('emitEventToApp', { eventName: 'webSocketMessage', eventData: data });
		},
		emitEventToApp: ({ state }, params) => {
			params.eventName = ((typeof (params.eventName) !== 'undefined') ? params.eventName : 'unknown');
			params.eventData = ((typeof (params.eventData) !== 'undefined') ? params.eventData : {});
			window.app.emit(params.eventName, params.eventData);
			if (window.ipc) { // Electron IPC...
				window.ipc.send(params.eventName, params.eventData);
			}
		},
		sendPageStatus: ({ state }, params) => {
			params = (typeof (params) !== 'undefined') ? params : {};
			params.action = 'status';
			if (!params.hasOwnProperty('page')) {
				params.page = window.app.views.main.router.url;
			}
			// if (params.page && params.page.includes('details'))
			// 	params.detailsLoad = true;
			state.mdWebSocket.sendOverWebSocket(params);
		},
		logout: ({ state }, params) => {
			state.mdWebSocket.closeWebSocket();
			window.localStorage.clear('session');
			state.session = {
				id: '',
				isLoggedIn: false,
				user: {}
			};
			window.app.panel.close('.panel-left', false);
			window.app.loginScreen.open('#my-login-screen', false);
		}
	},
})
export default store;
