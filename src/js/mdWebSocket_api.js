import ReconnectingWebSocket from '../lib/reconnecting-websocket/js/reconnecting-websocket-iife.js';

var mdWebSocket = function (hostFullURL, wsFullURL, protocol) {
	var thisClass = this;
	thisClass.host = hostFullURL;
	thisClass.hostWs = wsFullURL;
	thisClass.protocol = 'json';
	thisClass.sessionId = '';
	thisClass.ws = null;

	if ((Array.isArray(protocol)) || (typeof (protocol) == 'string')) {
		thisClass.protocol = protocol;
	}

	thisClass.ajaxToServer = function (requestObject) {
		requestObject = ((typeof (requestObject) == 'object') ? requestObject : {});

		var requestURL = '';
		var timeout = 10000;
		var dataObject = {};
		var showAjaxIndicator = true; // Default
		var cache = false;
		var beforeSendFunction = function () { };
		var completeFunction = function () { };
		var successFunction = function () { };
		// TODO: timeout function?
		var errorFunction = function (data) { console.log('Error', data); };
		var requestMethod = 'GET';

		// TODO: how to use... thisClass.sessionIdExpires

		if (requestObject.hasOwnProperty('url')) { // REQUIRED
			requestURL = requestObject.url;

			if (requestObject.hasOwnProperty('data')) {
				dataObject = requestObject.data;
			}

			if (requestObject.hasOwnProperty('showSpinner')) {
				showAjaxIndicator = requestObject.showSpinner;
			}

			if (requestObject.hasOwnProperty('timeout')) {
				timeout = requestObject.timeout;
			}

			if (requestObject.hasOwnProperty('cache')) {
				cache = requestObject.cache;
			}

			if (!cache) { // Force it...
				dataObject.noCache = Math.floor(Math.random() * 999999999);

			}

			// TODO: clean up showAjaxIndicator...
			if (requestObject.hasOwnProperty('beforeSend')) {
				if (showAjaxIndicator) {
					beforeSendFunction = function (xhr) {
						window.app.f7.preloader.show();
						return (requestObject.beforeSend(xhr));
					}
				} else {
					beforeSendFunction = requestObject.beforeSend;
				}
			} else {
				if (showAjaxIndicator) {
					beforeSendFunction = function (xhr) {
						window.app.f7.preloader.show();
						return true;
					}
				}
			}

			// TODO: clean up showAjaxIndicator...
			if (requestObject.hasOwnProperty('complete')) {
				if (showAjaxIndicator) {
					completeFunction = function (xhr, status) {
						requestObject.complete(xhr, status);
						window.app.f7.preloader.hide();
					}
				} else {
					completeFunction = requestObject.complete;
				}
			} else {
				if (showAjaxIndicator) {
					completeFunction = function (xhr, status) {
						window.app.f7.preloader.hide();
					}
				}
			}

			if (requestObject.hasOwnProperty('success')) {
				successFunction = requestObject.success;
			}

			if (requestObject.hasOwnProperty('error')) {
				errorFunction = requestObject.error;
			}

			if (requestObject.hasOwnProperty('method')) {
				requestMethod = requestObject.method;
			}

			var serverURL = this.host;
			if (requestURL.indexOf("http") > -1) {
				serverURL = requestURL;
			} else {
				serverURL += requestURL;
			}

			dataObject.sessionid = thisClass.sessionId; // necessary? Yes!
			dataObject.sessionId = thisClass.sessionId; // necessary? Yes!

			var tempCallBack = function (data) {
				/*
				if (data.hasOwnProperty('invalidsession')) {
					thisClass.isOnline= false; // TODO....
					
					var originalAjaxCallForInvalidSession= {
						url: serverURL,
						method: requestMethod,
						data: requestMethod == 'GET' ? dataObject : JSON.stringify(dataObject),
						dataType: 'json',
						beforeSend: beforeSendFunction,
						complete: completeFunction,
						success: successFunction,
						error: errorFunction
					};
					
					// Attempt to autoSignIn...
					//window.app.f7.methods.autoSignIn('/',originalAjaxCallForInvalidSession); // Don't go anywhere...?
					window.app.f7.methods.autoSignIn(''); // Don't go anywhere...?
				} else {
					successFunction(data);
				}
				*/
				successFunction(data);
			};

			window.app.f7.request({
				url: serverURL,
				method: requestMethod,
				data: requestMethod == 'GET' ? dataObject : JSON.stringify(dataObject),
				dataType: 'json',
				beforeSend: beforeSendFunction,
				complete: completeFunction,
				success: tempCallBack,
				error: errorFunction,
				timeout: timeout,
				xhrFields: {
					withCredentials: false
				}
			});

		} else {
			errorFunction({
				ok: false,
				error: 'URL required for ajaxToServer Calls'
			})
		}

	};

	thisClass.sendToServer = function (dataObject) {
		if (thisClass.ws) {
			if (thisClass.ws.readyState == thisClass.ws.OPEN) {
				thisClass.ws.send(JSON.stringify(dataObject));
				return true;
			} else {
				// TODO: fallback...? Send a POST...?
			}
		}
		return false;
	};

	thisClass.connectWebSocket = function (paramsObject) {
		paramsObject = ((typeof (paramsObject) == 'undefined') ? {} : paramsObject);

		if (!paramsObject.hasOwnProperty('onMessage')) {
			paramsObject.onMessage = function () { };
		}
		if (!paramsObject.hasOwnProperty('onOpen')) {
			paramsObject.onOpen = function () { };
		}
		if (!paramsObject.hasOwnProperty('onClose')) {
			paramsObject.onClose = function () { };
		}
		if (!paramsObject.hasOwnProperty('onTimeout')) {
			paramsObject.onTimeout = function () { };
		}
		if (!paramsObject.hasOwnProperty('timeout')) {
			paramsObject.timeout = 2000;
		}
		if (!paramsObject.hasOwnProperty('protocol')) {
			paramsObject.protocol = thisClass.protocol;
		}

		if (thisClass.ws !== null) {
			thisClass.ws.close();
		}

		var tempTimeout = setTimeout(function () {
			thisClass.ws = new ReconnectingWebSocket(wsFullURL, paramsObject.protocol);

			thisClass.ws.onopen = function (event) {
				thisClass.wsIsOnline = true;

				if (thisClass.timeoutVariable !== null) {
					clearTimeout(thisClass.timeoutVariable);
					thisClass.timeoutVariable = null;
				}

				paramsObject.onOpen(event);
			};

			thisClass.ws.onclose = function (event) {
				if (thisClass.wsIsOnline) {
					thisClass.wsIsOnline = false;
					paramsObject.onClose(event);

				} else {
					// Never connected, run the timeout function...
					if (thisClass.timeoutVariable !== null) {
						clearTimeout(thisClass.timeoutVariable);
						thisClass.timeoutVariable = null;
					}

					paramsObject.onTimeout(event);

				}
			};

			thisClass.ws.onmessage = paramsObject.onMessage;

			// TODO: use the reconnecting WebSocket library!
			/*
			thisClass.ws.onclose= function(event) {
				console.log('Websocket closed');
				thisClass.ws= null;
				
				var tempTimeout= setTimeout(function() {
					thisClass.connectWebSocket();
				}, 200);
			};
			*/
		}, 0);
	};
};

export default mdWebSocket;