import $ from 'dom7';
import Framework7 from 'framework7/bundle';

// Import F7 Styles
import 'framework7/css/bundle';

// Import Icons and App Custom Styles
import '../css/icons.css';
import '../css/app.css';

// Import Routes
import routes from './routes.js';
// Import Store
import store from './store.js';

// Import main app component
import App from '../app.f7';

window.app = new Framework7({
	// name: 'f7_mdWebSocketClient', // App name
	name: 'BidCalendar', // App name
	// theme: (device.desktop) ? 'aurora' : 'auto', // Automatic theme detection
	theme: 'auto', // Automatic theme detection
	el: '#app', // App root element
	component: App, // App main component
	id: 'com.hdfowler.f7_mdWebSocketClient', // App bundle ID

	// App store
	store: store,
	// App routes
	routes: routes,

	// autoDarkMode: true,

	view: {
		browserHistory: true
	},


	popover: {
		backdrop: false,
		closeOnEscape: true
	},

	on: {

		init: function () {
			var f7 = this;
			onAfterInit(f7);
		},

		pageInit: function () {
		}

	},
});

const onAfterInit = function (f7) {

	// OPEN LOGIN SCREEN RIGHT AWAY...

	f7.loginScreen.open('#my-login-screen');

	console.log(document.getElementById('my-login-screen'));

	if (true) { // TELL FRAMEWORK7 TO SEND CURRENT PAGE OVER WEBSOCKET
		window.lastEventNameAndPage = '';
		window.lastEventNameAndPageObject = { page: '', eventName: '' };

		f7.$(document).on('page:beforein popup:open popup:closed', function (e) {
			var pageURL = window.app.views.current.router.url;
			if (e.type !== 'popup:closed') {
				// If NOT an end... Get the pageURL from the element/route that triggered this event.
				if ((e.target) && (e.target.f7Component) && (e.target.f7Component.$route) && (e.target.f7Component.$route.url)) {
					pageURL = e.target.f7Component.$route.url;
				}
			}

			var thisEventAndPageObject = {
				page: pageURL,
				eventName: e.type
			};

			/*
			eventName: "page:beforein"
			page: "/"
			 */

			var thisEventAndPage = (thisEventAndPageObject.eventName + '-' + thisEventAndPageObject.page);

			if (thisEventAndPageObject.page !== window.lastEventNameAndPageObject.page) {
				if (!thisEventAndPageObject.page.endsWith('/select/')) {
					var alreadyHandledWithPageBeforeIn = ((thisEventAndPageObject.eventName == 'popup:closed') && (window.lastEventNameAndPage == 'page:beforein-' + thisEventAndPageObject.page));

					if (!alreadyHandledWithPageBeforeIn) {

						f7.store.dispatch('sendPageStatus');

						window.lastEventNameAndPage = thisEventAndPage;
						window.lastEventNameAndPageObject = thisEventAndPageObject;
					}
				}
			}
		});
	}
};

